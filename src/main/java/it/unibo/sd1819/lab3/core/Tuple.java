package it.unibo.sd1819.lab3.core;

public interface Tuple {
    default boolean matches(Template template) {
        return template.matches(this);
    }
}
