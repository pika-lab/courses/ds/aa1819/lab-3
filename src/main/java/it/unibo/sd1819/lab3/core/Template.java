package it.unibo.sd1819.lab3.core;

public interface Template {
    boolean matches(Tuple tuple);
}
