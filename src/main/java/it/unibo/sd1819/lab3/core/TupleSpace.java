package it.unibo.sd1819.lab3.core;

import java.util.concurrent.Future;

import org.apache.commons.collections4.MultiSet;

public interface TupleSpace<T extends Tuple, TT extends Template> {
    Future<T> read(TT template);
    
    Future<T> take(TT template);
    
    Future<T> write(T tuple);
    
    Future<MultiSet<? extends T>> get();
    
    Future<Integer> getSize();
    
    String getName();
}