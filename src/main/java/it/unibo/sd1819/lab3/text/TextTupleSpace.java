package it.unibo.sd1819.lab3.text;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.multiset.HashMultiSet;

import it.unibo.sd1819.lab3.core.TupleSpace;

public class TextTupleSpace implements TupleSpace<StringTuple, RegexTemplate> {
    
    private final MultiSet<StringTuple> tuples = new HashMultiSet<>();
    private final MultiSet<PendingRequest> pendingRequests = new HashMultiSet<>();
    private final ExecutorService executor;
    private final String name;
    
    public TextTupleSpace(final ExecutorService executor) {
        this(TextTupleSpace.class.getSimpleName(), executor);
    }
    
    public TextTupleSpace(final String name, final ExecutorService executor) {
        this.name = Objects.requireNonNull(name) + "#" + System.identityHashCode(this);
        this.executor = Objects.requireNonNull(executor);
    }
    
    public Future<StringTuple> read(final String regex) {
        return read(new RegexTemplate(regex));
    }
    
    public Future<StringTuple> read(final StringTuple tuple) {
        return read(tuple.getValue());
    }
    
    public Future<StringTuple> take(final String regex) {
        return take(new RegexTemplate(regex));
    }
    
    public Future<StringTuple> take(final StringTuple tuple) {
        return take(tuple.getValue());
    }
    
    public Future<StringTuple> write(final String regex) {
        return write(new StringTuple(regex));
    }
    
    @Override
    public Future<StringTuple> read(final RegexTemplate template) {
        log("Requested `read` operation on template: %s", template);
        final CompletableFuture<StringTuple> promise = new CompletableFuture<>();
        executor.execute(() -> this.handleRead(template, promise));
        return promise;
    }
    
    @Override
    public Future<StringTuple> take(final RegexTemplate template) {
        log("Requested `take` operation on template: %s", template);
        final CompletableFuture<StringTuple> promise = new CompletableFuture<>();
        executor.execute(() -> this.handleTake(template, promise));
        return promise;
    }
    
    @Override
    public Future<StringTuple> write(final StringTuple tuple) {
        log("Requested `write` operation for tuple: %s", tuple);
        final CompletableFuture<StringTuple> promise = new CompletableFuture<>();
        executor.execute(() -> this.handleWrite(tuple, promise));
        return promise;
    }
    
    @Override
    public Future<MultiSet<? extends StringTuple>> get() {
        log("Requested `get` operation");
        final CompletableFuture<MultiSet<? extends StringTuple>> promise = new CompletableFuture<>();
        executor.execute(() -> this.handleGet(promise));
        return promise;
    }
    
    @Override
    public Future<Integer> getSize() {
        log("Requested `getSize` operation");
        final CompletableFuture<Integer> promise = new CompletableFuture<>();
        executor.execute(() -> this.handleGetSize(promise));
        return promise;
    }       
    
    private synchronized void handleRead(final RegexTemplate template, final CompletableFuture<StringTuple> promise) {
        log("Handling `read` operation on template: %s", template);
        final Optional<StringTuple> matching = tuples.stream()
        		.filter(template::matches)
        		.findFirst();
        
        if (matching.isPresent()) {
            log("A tuple matching template %s was found: %s", template, matching.get());
            promise.complete(matching.get());
        } else {
            pendingRequests.add(new PendingRequest(RequestTypes.READ, template, promise));
            log("No tuple matching template %s was found, the `read` operation will be suspended", template);
        }

    }
    
    private synchronized void handleTake(final RegexTemplate template, final CompletableFuture<StringTuple> promise) {
    	throw new IllegalStateException("not implemented"); // TODO implement me
    }
    
    private synchronized void handleWrite(final StringTuple tuple, final CompletableFuture<StringTuple> promise) {
    	throw new IllegalStateException("not implemented"); // TODO implement me
    }
    
    private synchronized void handleGetSize(CompletableFuture<Integer> promise) {
        throw new IllegalStateException("not implemented"); // TODO implement me
     }

     private synchronized void handleGet(CompletableFuture<MultiSet<? extends StringTuple>> promise) {
     	throw new IllegalStateException("not implemented"); // TODO implement me
     }
    
    protected void log(String format, Object... args) {
        System.out.printf("[" + getName() + "] " + format + "\n", args);
    }

    public String getName() {
        return name;
    }
    
    @Override
	public String toString() {
		return "TextTupleSpace [name=" + name + "]";
	}
    
    // Helper classes
    
    private enum RequestTypes {
        READ, TAKE;
    }

	private static class PendingRequest {
    	private final RequestTypes requestType;
    	private final RegexTemplate template;
    	private final CompletableFuture<StringTuple> promise;
    	
		public PendingRequest(RequestTypes requestType, RegexTemplate template, CompletableFuture<StringTuple> promise) {
			this.requestType = Objects.requireNonNull(requestType);
			this.template = Objects.requireNonNull(template);
			this.promise = Objects.requireNonNull(promise);
		}

		public RequestTypes getRequestType() {
			return requestType;
		}

		public RegexTemplate getTemplate() {
			return template;
		}

		public CompletableFuture<StringTuple> getPromise() {
			return promise;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((promise == null) ? 0 : promise.hashCode());
			result = prime * result + ((requestType == null) ? 0 : requestType.hashCode());
			result = prime * result + ((template == null) ? 0 : template.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PendingRequest other = (PendingRequest) obj;
			if (promise == null) {
				if (other.promise != null)
					return false;
			} else if (!promise.equals(other.promise))
				return false;
			if (requestType != other.requestType)
				return false;
			if (template == null) {
				if (other.template != null)
					return false;
			} else if (!template.equals(other.template))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "PendingRequest [requestType=" + requestType + ", template=" + template + ", promise=" + promise + "]";
		}
    	
    }
}
