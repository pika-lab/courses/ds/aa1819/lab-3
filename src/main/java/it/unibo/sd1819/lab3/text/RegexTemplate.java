package it.unibo.sd1819.lab3.text;

import java.util.Objects;
import java.util.regex.Pattern;

import it.unibo.sd1819.lab3.core.Template;
import it.unibo.sd1819.lab3.core.Tuple;

public class RegexTemplate implements Template {
    
    private final Pattern regex;
    
    @Override
    public boolean matches(final Tuple tuple) {
        if (tuple instanceof StringTuple) {
            return regex.matcher(((StringTuple) tuple).getValue()).matches();
        }
        
        return false;
    }
    
    public RegexTemplate(final String regex) {
        Objects.requireNonNull(regex);
        this.regex = Pattern.compile(regex, Pattern.MULTILINE);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (regex == null ? 0 : regex.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegexTemplate other = (RegexTemplate) obj;
        if (regex == null) {
            if (other.regex != null) {
                return false;
            }
        } else if (!regex.equals(other.regex)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "/" + regex.pattern() + "/";
    }
    
}
