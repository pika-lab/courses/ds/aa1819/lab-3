package it.unibo.sd1819.lab3;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;

import it.unibo.sd1819.test.Counter;

public class TestFutures {
    
    private ExecutorService executor;
    
    @Before
    public void setUp() throws Exception {
        executor = Executors.newSingleThreadExecutor();
    }
    
    private BigInteger factorial(long value) {
        final BigInteger limit = BigInteger.valueOf(value);
        BigInteger x = BigInteger.ONE;
        for (BigInteger i = BigInteger.ONE; i.compareTo(limit) <= 0; i = i.add(BigInteger.ONE)) {
            x = x.multiply(i);
            System.out.printf("fact(%s) = %s\n", i, x);
        }
        return x;
    }
    
    @Test
    public void testAsynchronousRunnable() {
        final Counter x = new Counter();
        
        assertEquals(0, x.getValue());
        
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) { }
                x.inc();
            }
        });
        
        assertEquals(0, x.getValue());
        
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) { }
        
        assertEquals(1, x.getValue());
    }
    
    @Test
    public void testAsynchronousCallable() {
        final long computeFactorialOf = 100;
        final BigInteger expected = new BigInteger("93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000");
        
        final Future<BigInteger> result = executor.submit(new Callable<BigInteger>() {

            @Override
            public BigInteger call() throws Exception {
                return factorial(computeFactorialOf);
            }
        });
        
        System.out.println("Should appear soon");

        assertFalse(result.isDone());
        
        System.out.println("Should appear soon, too");
        
        try {
            assertEquals(expected, result.get());
            System.out.println("Should appear after a while");
        } catch (InterruptedException | ExecutionException e) {
            fail();
        }
    }
    
    @Test
    public void testPromise() {
        final CompletableFuture<Integer> promise = new CompletableFuture<>();
        final Future<Integer> result = promise;
        
        executor.execute(() -> {
            final Random rand = new Random(); 
            try {
                Thread.sleep(rand.nextInt(5000));
                promise.complete(rand.nextInt(10));
            } catch (InterruptedException e) { 
                promise.completeExceptionally(e);
            }
            
        });
        
        assertFalse(result.isDone());
        
        System.out.println("Should appear soon");
        
        try {
            assertTrue(result.get() <= 10);
            System.out.println("Should appear after a while");
        } catch (InterruptedException | ExecutionException e) {
            fail();
        }
    }
}
