package it.unibo.sd1819.lab3;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class TestExamples {
    
    @Test
    public void executeExample() throws InterruptedException {
        final ExecutorService ex = Executors.newCachedThreadPool();
        ex.execute(new Runnable() {
            
            @Override
            public void run() {
                System.out.println("Hello");
            }
        });
        
        System.out.println("This may be printed before 'Hello'");
        
        ex.shutdown();
        ex.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }
    
    @Test
    public void submitExample() throws InterruptedException, ExecutionException {
        final ExecutorService ex = Executors.newCachedThreadPool();
        final Future<Integer> result = ex.submit(new Callable<Integer>() {

            @Override
            public Integer call() throws Exception {
                return 1 + 2;
            }
            
        });
        
        System.out.println("This may be printed before the result");
        System.out.println("Future result: " + result);
        System.out.println("Is future result done? " + result.isDone());
        System.out.println("result: " + result.get());
        
        ex.shutdown();
        ex.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }
    
    @Test
    public void promise() throws InterruptedException, ExecutionException {
        final ExecutorService ex = Executors.newCachedThreadPool();
        final Random rand = new Random();
        final CompletableFuture<Integer> promise = new CompletableFuture<Integer>();
        final int toBeFound = 100;
        
        while (!promise.isDone()) {
            ex.execute(() -> {
                int guess = rand.nextInt(toBeFound + 1);
                if (guess == toBeFound) {
                    System.out.printf("Guess: %d. FOUND!\n", guess);
                    promise.complete(guess);
                } else {
                    System.out.printf("Guess: %d.\n", guess);
                }
            });
        }
        
        System.out.println("Result: " + promise.get());
        
        ex.shutdown();
        ex.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }
}
