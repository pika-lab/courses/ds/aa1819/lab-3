package it.unibo.sd1819.lab3.text;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.multiset.HashMultiSet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import it.unibo.sd1819.lab3.ActiveObject;
import it.unibo.sd1819.test.ConcurrentTestHelper;

public class TestTextTupleSpace {
    
    private static ExecutorService executor;

    @BeforeClass
    public static void setUpUnit() throws Exception {
        executor = Executors.newSingleThreadExecutor();
    }
    
    private TextTupleSpace tupleSpace;
    private ConcurrentTestHelper test;
    private Random rand;
    
    @Before
    public void setUp() throws Exception {
        tupleSpace = new TextTupleSpace(executor);
        test = new ConcurrentTestHelper();
        rand = new Random();
    }
    
    @Test
    public void testInitiallyEmpty() throws Exception {
    	test.setThreadCount(1);
    	
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.getSize(), 0, "The tuple space must initially be empty");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    
    @Test
    public void testReadSuspensiveSemantics() throws Exception {
    	test.setThreadCount(1);
    	
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertBlocksIndefinitely(tupleSpace.read(".*"), "A read operation should block if no tuple matching the requested template is available");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    @Test
    public void testTakeSuspensiveSemantics() throws Exception {
    	test.setThreadCount(1);
    	
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertBlocksIndefinitely(tupleSpace.take(".*"), "A take operation should block if no tuple matching the requested template is available");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    
    
    @Test
    public void testWriteGenerativeSemantics() throws Exception {
    	test.setThreadCount(1);
    	
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.getSize(), 0, "The tuple space must initially be empty");
                test.assertEquals(tupleSpace.write("foo"), new StringTuple("foo"), "A write operation eventually return the same tuple it received as argument");
                test.assertEquals(tupleSpace.getSize(), 1, "After a tuple was written, the tuple space size should increase");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    @Test
    public void testReadIsIdempotent() throws Exception {
    	test.setThreadCount(2);
    	
        final StringTuple tuple = new StringTuple("foo bar");
        
        final ActiveObject bob = new ActiveObject("Bob") {
            
            @Override 
            protected void loop() throws Exception {
                for (int i = rand.nextInt(10) + 1; i >= 0; i--) {
                    test.assertEquals(tupleSpace.read(".*?foo.*"), tuple);
                }
                test.assertEquals(tupleSpace.read(".*?bar.*"), tuple);
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEventuallyReturns(tupleSpace.write(tuple));
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
    }
    
    @Test
    public void testTakeIsNotIdempotent2() throws Exception {
    	test.setThreadCount(2);
    	
        final StringTuple tuple = new StringTuple("foo bar");
        
    	final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.write(tuple));
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        };
        
        final ActiveObject bob = new ActiveObject("Bob") {
            
            @Override 
            protected void loop() throws Exception {
            	final Future<StringTuple> toBeTaken = tupleSpace.take(".*?foo.*");
            	alice.start();
                test.assertEquals(toBeTaken, tuple);
                test.assertBlocksIndefinitely(tupleSpace.take(".*?bar.*"));
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
    }
    
    @Test
    public void testTakeIsNotIdempotent1() throws Exception {
    	test.setThreadCount(2);
    	
        final StringTuple tuple = new StringTuple("foo bar");
        
        final ActiveObject bob = new ActiveObject("Bob") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.take(".*?foo.*"), tuple);
                test.assertBlocksIndefinitely(tupleSpace.take(".*?bar.*"));
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        };
        
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.write(tuple));
            	bob.start();
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
    }
    
    @Test
    public void testAssociativeAccess() throws Exception {
    	test.setThreadCount(3);
    	
        final StringTuple tuple4Bob = new StringTuple("recipient: bob");
        final StringTuple tuple4Carl = new StringTuple("recipient: carl");
        
        final ActiveObject carl = new ActiveObject("Carl") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.read(".*?carl.*"), tuple4Carl);
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        final ActiveObject bob = new ActiveObject("Bob") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.read(".*?bob.*"), tuple4Bob);
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.write(tuple4Bob));
            	test.assertEventuallyReturns(tupleSpace.write(tuple4Carl));
                
                test.assertOneOf(tupleSpace.take("recipient:.*"), tuple4Bob, tuple4Carl);
                test.assertOneOf(tupleSpace.take("recipient:.*"), tuple4Bob, tuple4Carl);
                
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
        carl.await();
    }
    
    @Test
    public void testGetSize() throws Exception {
    	test.setThreadCount(1);
        
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEquals(tupleSpace.getSize(), 0);
            	test.assertEventuallyReturns(tupleSpace.write("a"));
            	test.assertEquals(tupleSpace.getSize(), 1);
            	test.assertEventuallyReturns(tupleSpace.write("a"));
            	test.assertEquals(tupleSpace.getSize(), 2);
            	test.assertEventuallyReturns(tupleSpace.write("a"));
            	test.assertEquals(tupleSpace.getSize(), 3);
            	
            	test.assertEventuallyReturns(tupleSpace.take("a"));
            	test.assertEquals(tupleSpace.getSize(), 2);
                
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    @Test
    public void testGetAll() throws Exception {
    	test.setThreadCount(1);
    	
    	final MultiSet<StringTuple> expected = new HashMultiSet<>(Arrays.asList(
    				new StringTuple("b"),
    				new StringTuple("c"),
    				new StringTuple("a"),
    				new StringTuple("b")
    			));
        
        final ActiveObject alice = new ActiveObject("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.write("a"));
            	test.assertEventuallyReturns(tupleSpace.write("b"));
            	test.assertEventuallyReturns(tupleSpace.write("b"));
            	test.assertEventuallyReturns(tupleSpace.write("c"));
            	
            	test.assertEquals(tupleSpace.get(), expected);
                
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
}
